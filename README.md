# so_long_map_gen

Side project for the main project so_long. Wanted big map but lazy to build up by hand.
So here is a procedural maze map generator for the projet so_long from 42.

## Specification

Made for a top-down view
I use recursive division method to create the maze, see this on wikipedia
and wanted to work on recursive, [Recursive division method](https://en.wikipedia.org/wiki/Maze_generation_algorithm#Recursive_division_method)

## Usage

### Install

`git clone https://gitlab.com/Joqu3r/so_long_map_gen.git`

### Makefile

`make` : Compile src and create binary

`make clean` : Delete all object and depths

`make fclean` : Run clean rule and delete binary

###	Binary

You must specified the width and height of the map you want, within range [10, 10000]

`./so_long_map_gen [width] [height]`

The program will create the map and then give you the relative path of the newly created map.

## Modification

You can change some parameter in include/so_long_map_gen.h

ITEMS_LIMIT define the limited number of item by maps (5000 * 5000 give 27000 item if no limit)

PATH		define the path where the maps will be created, make sure directory exist, else it will display an error
