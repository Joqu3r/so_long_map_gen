/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long_map_gen.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 21:28:16 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/15 20:43:51 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

static int	write_map_file(char **map, char *file)
{
	int	fd;
	int	i;

	fd = open(file, O_WRONLY | O_APPEND | O_CREAT, \
	S_IRWXU | S_IRGRP | S_IROTH);
	if (fd == -1)
	{	
		perror("write_map_file");
		return (1);
	}
	i = 0;
	while (map[i])
	{
		ft_putendl_fd(map[i], fd);
		i++;
	}
	close(fd);
	return (0);
}

static char	*create_name_map_file(void)
{
	char	*tmp;
	char	*name;
	int		n;

	n = 12;
	name = ft_calloc(n + 1, sizeof(*tmp));
	if (!name)
		return (name);
	while (--n >= 0)
		name[n] = (rand() % (91 - 65) + 65);
	tmp = ft_strjoin(name, "_sl_map_gen.ber");
	free(name);
	if (!tmp)
		return (tmp);
	name = ft_strjoin(PATH, tmp);
	free(tmp);
	return (name);
}

int	free_strs(char **strs)
{
	int	i;

	i = 0;
	if (!strs)
		return (1);
	while (strs[i])
	{
		free(strs[i]);
		i++;
	}
	free(strs);
	return (0);
}

int	print_error(int error, char *where)
{
	if (error == -1)
		perror(where);
	ft_putendl_fd("Error", 2);
	if (error == E_ARG)
	{
		ft_putstr_fd("You must specify width and height (both within", 2);
		ft_putstr_fd(" [10, 10000]", 2);
		ft_putendl_fd(" range) as follow:\n./so_long [width] [height]", 2);
	}
	return (1);
}

int	main(int ac, char **av)
{
	char	**map;
	char	*file;

	if (ac != 3)
		return (print_error(E_ARG, NULL));
	srand(time(NULL));
	map = create_map(ft_atoi(av[1]), ft_atoi(av[2]));
	if (!map)
		return (1);
	file = create_name_map_file();
	if (!file)
	{
		free_strs(map);
		return (print_error(-1, "main"));
	}
	if (write_map_file(map, file))
	{
		free(file);
		free_strs(map);
		return (1);
	}
	ft_printf("%s has been created.\n", file);
	free(file);
	free_strs(map);
	return (0);
}
