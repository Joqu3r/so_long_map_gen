/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   random_wall.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/07 10:21:36 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/07 10:29:36 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

void	random_wall_x(char **map, t_dim dim, int *x)
{
	int	c;

	c = 0;
	while ((map[dim.start.y][*x] == '0' || map[dim.end.y][*x] == '0')
		&& dim.end.x - dim.start.x > 3)
	{
		if (c > 9)
		{
			*x = -1;
			break ;
		}
		*x = rand() % (dim.end.x - (dim.start.x + 3)) + \
		(dim.start.x + 2);
		c++;
	}
}

void	random_wall_y(char **map, t_dim dim, int *y)
{
	int	c;

	c = 0;
	while ((map[*y][dim.start.x] == '0' || map[*y][dim.end.x] == '0')
		&& dim.end.y - dim.start.y > 3)
	{
		if (c > 9)
		{
			*y = -1;
			break ;
		}
		*y = rand() % (dim.end.y - (dim.start.y + 3)) + \
		(dim.start.y + 2);
		c++;
	}
}
