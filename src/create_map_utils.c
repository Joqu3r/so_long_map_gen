/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/07 10:06:43 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/07 10:07:00 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

t_pos	init_pos(int x, int y)
{
	t_pos	pos;

	pos.x = x;
	pos.y = y;
	return (pos);
}

int	count_empty_space(char **map)
{
	int	i;
	int	y;
	int	c;

	c = 0;
	i = 0;
	while (map[i])
	{
		y = 0;
		while (map[i][y])
		{
			if (map[i][y] == '0')
				c++;
			y++;
		}
		i++;
	}
	return (c);
}

void	set_player(char **map, int x, int y)
{
	t_pos	random;

	random = init_pos(rand() % (x - 1) + 1, rand() % (y - 1) + 1);
	while (map[random.y][random.x] != '0')
		random = init_pos(rand() % (x - 1) + 1, rand() % (y - 1) + 1);
	map[random.y][random.x] = 'P';
}
