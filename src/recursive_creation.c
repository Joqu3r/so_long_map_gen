/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_creation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 22:44:41 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/07 10:29:16 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

static void	open_line(char **map, t_dim dim, t_pos lines)
{
	if (lines.y < 0)
	{
		open_wall(map, init_pos(lines.x, dim.start.y), \
		init_pos(lines.x, dim.end.y));
	}
	else
	{
		open_wall(map, init_pos(dim.start.x, lines.y), \
		init_pos(dim.end.x, lines.y));
	}
}

static void	open_sections(char **map, t_dim dim, t_pos lines, t_pos cross)
{
	int	random;

	if (cross.x == -1 || cross.y == -1)
		return (open_line(map, dim, lines));
	random = rand() % 4;
	if (random != 0)
		open_wall(map, init_pos(dim.start.x, lines.y), \
		init_pos(cross.x, lines.y));
	if (random != 1)
		open_wall(map, init_pos(cross.x, lines.y), \
		init_pos(dim.end.x, lines.y));
	if (random != 2)
		open_wall(map, init_pos(lines.x, dim.start.y), \
		init_pos(lines.x, cross.y));
	if (random != 3)
		open_wall(map, init_pos(lines.x, cross.y), \
		init_pos(lines.x, dim.end.y));
}

static void	write_lines(char **map, t_dim dim, int x, int y)
{
	int	j;
	int	k;

	j = dim.start.y;
	while (j <= dim.end.y && x > 0)
	{
		map[j][x] = '1';
		j++;
	}
	k = dim.start.x;
	while (k <= dim.end.x && y > 0)
	{
		map[y][k] = '1';
		k++;
	}
}

static void	calculate_rand_lines(char **map, t_dim dim, t_pos *lines)
{
	int	randx;
	int	randy;

	randx = rand() % (dim.end.x - (dim.start.x + 3)) + \
	(dim.start.x + 2);
	randy = rand() % (dim.end.y - (dim.start.y + 3)) + \
	(dim.start.y + 2);
	random_wall_x(map, dim, &randx);
	random_wall_y(map, dim, &randy);
	lines->x = randx;
	lines->y = randy;
}

void	recursive_creation(char **map, t_dim dim)
{
	t_pos	lines;
	t_pos	cross;

	if (dim.end.x - dim.start.x < 4 || dim.end.y - dim.start.y < 4)
		return ;
	calculate_rand_lines(map, dim, &lines);
	write_lines(map, dim, lines.x, lines.y);
	cross = get_intersection(map, dim);
	open_sections(map, dim, lines, cross);
	recursive_call(map, dim, lines, cross);
}
