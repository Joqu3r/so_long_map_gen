/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_creation_utils.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 14:54:05 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/07 10:07:22 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

static t_dim	init_dim(int sx, int sy, int ex, int ey)
{
	t_dim	dim;

	dim.start = init_pos(sx, sy);
	dim.end = init_pos(ex, ey);
	return (dim);
}

static void	do_one_call(char **map, t_dim dim, t_pos lines)
{	
	if (lines.y < 0)
	{
		recursive_creation(map, init_dim(dim.start.x, dim.start.y, \
		lines.x, dim.end.y));
	}
	else
	{
		recursive_creation(map, init_dim(dim.start.x, dim.start.y, \
		dim.end.x, lines.y));
	}
}

void	recursive_call(char **map, t_dim dim, t_pos lines, t_pos cross)
{
	if (cross.x == -1 || cross.y == -1)
	{
		do_one_call(map, dim, lines);
		return ;
	}
	recursive_creation(map, init_dim(dim.start.x, dim.start.y, \
	cross.x, cross.y));
	recursive_creation(map, init_dim(cross.x, dim.start.y, \
	dim.end.x, cross.y));
	recursive_creation(map, init_dim(dim.start.x, cross.y, \
	cross.x, dim.end.y));
	recursive_creation(map, init_dim(cross.x, cross.y, \
	dim.end.x, dim.end.y));
}

void	open_wall(char **map, t_pos start, t_pos end)
{
	int	random;
	int	strt;
	int	ed;

	if (start.y == end.y)
	{
		strt = start.x;
		ed = end.x;
	}
	else
	{
		strt = start.y;
		ed = end.y;
	}
	if (start.x < 0)
		start.x *= -1;
	if (start.y < 0)
		start.y *= -1;
	random = rand() % (ed - strt - (ed - strt > 1)) + strt + (ed - strt > 1);
	if (start.y == end.y)
		map[start.y][random] = '0';
	else
		map[random][start.x] = '0';
}

t_pos	get_intersection(char **map, t_dim dim)
{
	t_pos	pos;

	pos.y = dim.start.y + 1;
	while (pos.y <= dim.end.y - 1)
	{
		pos.x = dim.start.x + 1;
		while (pos.x <= dim.end.x - 1)
		{
			if (map[pos.y][pos.x] == '1' &&
			(map[pos.y][pos.x + 1] == '1' && map[pos.y][pos.x - 1] == '1') &&
			(map[pos.y + 1][pos.x] == '1' && map[pos.y - 1][pos.x] == '1'))
				return (pos);
			pos.x++;
		}
		pos.y++;
	}
	pos.x = -1;
	pos.y = -1;
	return (pos);
}
