/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 21:47:09 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/07 11:53:46 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_map_gen.h"

static void	set_items(char **map, int x, int y)
{
	t_pos	random;
	int		c;

	c = 5 * (count_empty_space(map)) / 100;
	if (c > ITEMS_LIMIT)
		c = ITEMS_LIMIT;
	while (c > 0)
	{
		random = init_pos(rand() % (x - 1) + 1, rand() % (y - 1) + 1);
		while (map[random.y][random.x] != '0')
			random = init_pos(rand() % (x - 1) + 1, rand() % (y - 1) + 1);
		map[random.y][random.x] = 'C';
		c--;
	}
}

static void	set_end(char **map, int x, int y)
{
	int	random;

	random = rand() % 4;
	if (random == 0)
		map[1][1] = 'E';
	if (random == 1)
		map[1][x - 1] = 'E';
	if (random == 2)
		map[y - 1][1] = 'E';
	if (random == 3)
		map[y - 1][x - 1] = 'E';
}

static void	create_wall(char **map, int x, int y)
{
	t_dim	dim;

	dim.start = init_pos(0, 0);
	dim.end = init_pos(x - 1, y - 1);
	recursive_creation(map, dim);
}

static int	create_layout(char **layout, int x, int y)
{
	int		j;
	int		k;

	j = 0;
	while (j < y)
	{
		layout[j] = ft_calloc(x + 1, sizeof(**layout));
		if (!layout[j])
		{
			free_strs(layout);
			return (print_error(-1, "create_layout"));
		}
		k = 0;
		while (k < x)
		{
			if ((j == 0 || j == y - 1) || (k == 0 || k == x - 1))
				layout[j][k] = '1';
			else
				layout[j][k] = '0';
			k++;
		}
		j++;
	}
	layout[j] = NULL;
	return (0);
}

char	**create_map(int x, int y)
{
	char	**map;

	if ((x < 10 || x > 10000) || (y < 10 || x > 10000))
	{
		print_error(E_ARG, NULL);
		return (NULL);
	}
	map = ft_calloc(y + 1, sizeof(*map));
	if (!map)
	{
		print_error(-1, "create_map");
		return (map);
	}
	if (create_layout(map, x, y))
	{
		print_error(-1, "create_map");
		free_strs(map);
		return (NULL);
	}
	create_wall(map, x, y);
	set_end(map, x - 1, y - 1);
	set_items(map, x - 1, y - 1);
	set_player(map, x - 1, y - 1);
	return (map);
}
