/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long_map_gen.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 21:37:29 by thugueno          #+#    #+#             */
/*   Updated: 2023/03/14 14:25:29 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_MAP_GEN_H
# define SO_LONG_MAP_GEN_H

# include <time.h>
# include <stdio.h>
# include <errno.h>
# include "libft_ft_printf.h"
# include "libft_file.h"

/*	can be modified	*/

# define ITEMS_LIMIT 999
# define PATH "gen/"

/*	---------------	*/

# define E_ARG 0
# define E_FCREATE 1

typedef struct s_pos
{
	int	x;
	int	y;
}	t_pos;

typedef struct s_dim
{
	t_pos	start;
	t_pos	end;
}	t_dim;

int		print_error(int error, char *where);
int		free_strs(char **strs);
char	**create_map(int x, int y);
void	recursive_creation(char **map, t_dim dim);
void	random_wall_x(char **map, t_dim dim, int *x);
void	random_wall_y(char **map, t_dim dim, int *y);
t_pos	get_intersection(char **map, t_dim dim);
t_pos	init_pos(int x, int y);
void	open_wall(char **map, t_pos start, t_pos end);
void	recursive_call(char **map, t_dim dim, t_pos lines, t_pos cross);
int		count_empty_space(char **map);
void	set_player(char **map, int x, int y);

#endif
